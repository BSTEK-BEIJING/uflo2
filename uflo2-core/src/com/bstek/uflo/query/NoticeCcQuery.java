 
package com.bstek.uflo.query;

import java.util.Date;
import java.util.List;

import com.bstek.uflo.model.task.TaskNoticeCc;

 
public interface NoticeCcQuery extends Query<List<TaskNoticeCc>>{
	NoticeCcQuery id(long id);
	NoticeCcQuery processId(long processId);
	NoticeCcQuery businessId(String businessId);
	NoticeCcQuery userId(String userId);
	NoticeCcQuery taskNameLike(String taskNameLike);
	NoticeCcQuery createDateLessThen(Date date);
	NoticeCcQuery createDateLessThenOrEquals(Date date);
	NoticeCcQuery createDateGreaterThen(Date date);
	NoticeCcQuery createDateGreaterThenOrEquals(Date date);
	NoticeCcQuery page(int firstResult, int maxResults);
	NoticeCcQuery addOrderAsc(String property);
	NoticeCcQuery addOrderDesc(String property);
}

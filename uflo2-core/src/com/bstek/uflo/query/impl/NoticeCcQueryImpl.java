 
package com.bstek.uflo.query.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.bstek.uflo.command.CommandService;
import com.bstek.uflo.command.impl.QueryCountCommand;
import com.bstek.uflo.command.impl.QueryListCommand;
import com.bstek.uflo.model.task.TaskNoticeCc;
import com.bstek.uflo.query.NoticeCcQuery;
import com.bstek.uflo.query.QueryJob;

 
public class NoticeCcQueryImpl implements NoticeCcQuery,QueryJob{
	private long id;
	private String taskNameLike;
	private String userId;
	private long processId;
	private String businessId;
	private int firstResult;
	private int maxResults;
	private Date createDateLessThen;
	private Date createDateLessThenOrEquals;
	private Date createDateGreaterThen;
	private Date createDateGreaterThenOrEquals;
	private List<String> ascOrders=new ArrayList<String>();
	private List<String> descOrders=new ArrayList<String>();
	private CommandService commandService;
	public NoticeCcQueryImpl(CommandService commandService){
		this.commandService=commandService;
	}
	public Criteria getCriteria(Session session,boolean queryCount) {
		Criteria criteria=session.createCriteria(TaskNoticeCc.class);
		buildCriteria(criteria,queryCount);
		return criteria;
	}
	
	public List<TaskNoticeCc> list() {
		return commandService.executeCommand(new QueryListCommand<List<TaskNoticeCc>>(this));
	}

	public int count() {
		return commandService.executeCommand(new QueryCountCommand(this));
	}

	private void buildCriteria(Criteria criteria,boolean queryCount){
		 
		if(!queryCount && firstResult>0){
			criteria.setFirstResult(firstResult);			
		}
		if(!queryCount && maxResults>0){
			criteria.setMaxResults(maxResults);			
		}
		if(id>0){
			criteria.add(Restrictions.eq("id", id));
		}
		if(StringUtils.isNotEmpty(taskNameLike)){
			criteria.add(Restrictions.like("taskName", taskNameLike));
		}
		if(StringUtils.isNotEmpty(userId)){
			criteria.add(Restrictions.in("userId", Arrays.asList(userId)));
		}
		if(StringUtils.isNotEmpty(businessId)){
			criteria.add(Restrictions.eq("businessId",businessId));
		}
		if(processId>0){
			criteria.add(Restrictions.eq("processId",processId));
		}
	 
		if(createDateLessThen!=null){
			criteria.add(Restrictions.lt("createDate", createDateLessThen));
		}
		if(createDateGreaterThen!=null){
			criteria.add(Restrictions.gt("createDate", createDateGreaterThen));
		}
		if(createDateLessThenOrEquals!=null){
			criteria.add(Restrictions.le("createDate", createDateLessThenOrEquals));
		}
		if(createDateGreaterThenOrEquals!=null){
			criteria.add(Restrictions.ge("createDate", createDateGreaterThenOrEquals));
		}
 
		if(!queryCount){
			for(String ascProperty:ascOrders){
				criteria.addOrder(Order.asc(ascProperty));
			}
			for(String descProperty:descOrders){
				criteria.addOrder(Order.desc(descProperty));
			}
		}
	}
	
	public NoticeCcQuery createDateGreaterThen(Date date) {
		this.createDateGreaterThen=date;
		return this;
	}
	public NoticeCcQuery createDateGreaterThenOrEquals(Date date) {
		this.createDateGreaterThenOrEquals=date;
		return this;
	}
	public NoticeCcQuery createDateLessThen(Date date) {
		this.createDateLessThen=date;
		return this;
	}
	public NoticeCcQuery createDateLessThenOrEquals(Date date) {
		this.createDateLessThenOrEquals=date;
		return this;
	}
	
	public NoticeCcQuery addOrderAsc(String property){
		ascOrders.add(property);
		return this;
	}

	public NoticeCcQuery addOrderDesc(String property){
		descOrders.add(property);
		return this;
	}
	
	public NoticeCcQuery id(long id) {
		this.id=id;
		return this;
	}

	public NoticeCcQuery page(int firstResult, int maxResults) {
		this.firstResult=firstResult;
		this.maxResults=maxResults;
		return this;
	}
	
	@Override
	public NoticeCcQuery processId(long processId) {
		this.processId=processId;
		return this;
	}
 
	@Override
	public NoticeCcQuery businessId(String businessId) {
		this.businessId=businessId;
		return this;
	}
	 
	@Override
	public NoticeCcQuery userId(String userId) {
		this.userId=userId;
		return this;
	}
	@Override
	public NoticeCcQuery taskNameLike(String taskNameLike) {
		this.taskNameLike=taskNameLike;
		return this;
	}

	
	
}

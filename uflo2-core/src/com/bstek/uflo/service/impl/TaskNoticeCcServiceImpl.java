 
package com.bstek.uflo.service.impl;

import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.bstek.uflo.command.CommandService;
import com.bstek.uflo.command.impl.DeleteProcessVariableCommand;
import com.bstek.uflo.command.impl.GetProcessByKeyCommand;
import com.bstek.uflo.command.impl.GetProcessCommand;
import com.bstek.uflo.command.impl.GetTaskNoticeCcCommand;
import com.bstek.uflo.model.ProcessDefinition;
import com.bstek.uflo.model.task.TaskNoticeCc;
import com.bstek.uflo.query.NoticeCcQuery;
import com.bstek.uflo.query.impl.NoticeCcQueryImpl;
import com.bstek.uflo.service.TaskNoticeCcService;


public class TaskNoticeCcServiceImpl implements TaskNoticeCcService,ApplicationContextAware{
 
	private CommandService commandService;
	private NoticeCcQuery noticeCcQuery;
	
	@Override
	public NoticeCcQuery loadProcessDefinition(Integer start, 
			Integer end) {
		NoticeCcQuery page = createQuery().page(start, end);
	 
		page.addOrderAsc("createDate");
		return page;
	}
	@Override
	public List<TaskNoticeCc> getTaskNoticeCcByProcessId(long processId) {
		  List<TaskNoticeCc> executeCommand = commandService.
				  executeCommand(new GetTaskNoticeCcCommand(processId));
		return executeCommand;
	}

	
	@Override
	public NoticeCcQuery createQuery() {
		return new NoticeCcQueryImpl(commandService);
	}
	@Override
	public List<TaskNoticeCc> getTaskNoticeCcByTaskName(String taskName) {
		List<TaskNoticeCc> list = noticeCcQuery.taskNameLike(taskName).list();
		  List<TaskNoticeCc> executeCommand = commandService.
				  executeCommand(new GetTaskNoticeCcCommand(taskName,""));
		return executeCommand;
	}
	
	@Override
	public List<TaskNoticeCc> getTaskNoticeCcByBusinessId(String businessId) {
		List<TaskNoticeCc> executeCommand = commandService.
				executeCommand(new GetTaskNoticeCcCommand("",businessId));
		return executeCommand;
	}

 
	public void deleteProcessVariable(String key, long processInstanceId) {
		commandService.executeCommand(new DeleteProcessVariableCommand(key,processInstanceId));
	}
	
	public ProcessDefinition getProcessByKey(String key,boolean isparseProcess) {
		ProcessDefinition process=commandService.executeCommand(new GetProcessByKeyCommand(key,isparseProcess));
		return process;
	}
 

	public ProcessDefinition getProcessByName(String processName) {
		return commandService.executeCommand(new GetProcessCommand(0,processName,0,null));
	}
	
	public ProcessDefinition getProcessByName(String processName,String categoryId) {
		return commandService.executeCommand(new GetProcessCommand(0,processName,0,categoryId));
	}
 

	public void setCommandService(CommandService commandService) {
		this.commandService = commandService;
	}


 
 

 
 
 
	
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		 
	}




	
 



	
}

 
package com.bstek.uflo.service;

import java.util.List;

import com.bstek.uflo.model.task.TaskNoticeCc;
import com.bstek.uflo.query.NoticeCcQuery;

 
public interface TaskNoticeCcService {
	public static final String BEAN_ID="uflo.processService";
	
	NoticeCcQuery loadProcessDefinition(Integer start, 
			Integer end);
 
	List<TaskNoticeCc> getTaskNoticeCcByProcessId(long processId);
 
 
	List<TaskNoticeCc> getTaskNoticeCcByTaskName(String taskName);
	
	
	List<TaskNoticeCc> getTaskNoticeCcByBusinessId(String businessId);
 
	/**
	 * @return 创建创建成功的流程模版查询对象
	 */
	NoticeCcQuery createQuery();
	
	 
	 
}

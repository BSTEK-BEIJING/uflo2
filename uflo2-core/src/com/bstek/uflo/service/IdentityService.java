/*******************************************************************************
 * Copyright 2017 Bstek
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.bstek.uflo.service;

import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;

import java.util.Collection;

/**
 * 任务处理人选择接口
 *
 * @author Jacky.gao
 * @since 2013年8月12日
 */
public interface IdentityService {
	public static final String USER_TYPE = "user";
	public static final String DEPT_TYPE = "udept";
	public static final String POSITION_TYPE = "position";
	public static final String GROUP_TYPE = "group";
	public static final String DEPT_POSITION_TYPE = "dept-position";

	public static final String BEAN_ID = "uflo.identityService";

	/**
	 * 用户分页查询
	 *
	 * @param query
	 */
	void userPageQuery(PageQuery<Entity> query);

	/**
	 * 机构分页查询
	 *
	 * @param query
	 * @param parentId
	 */
	void deptPageQuery(PageQuery<Entity> query, String parentId);

	/**
	 * 岗位分页查询
	 *
	 * @param query
	 * @param parentId
	 */
	void positionPageQuery(PageQuery<Entity> query, String parentId);

	/**
	 * 群组分页查询
	 *
	 * @param query
	 * @param parentId
	 */
	void groupPageQuery(PageQuery<Entity> query, String parentId);

	/**
	 * 查询某一群组下的用户列表
	 *
	 * @param group
	 * @return
	 */
	Collection<String> getUsersByGroup(String group);

	/**
	 * 查询某一岗位下的用户列表
	 *
	 * @param position
	 * @return
	 */
	Collection<String> getUsersByPosition(String position);

	/**
	 * 查询某一部门下的用户列表
	 *
	 * @param dept
	 * @return
	 */
	Collection<String> getUsersByDept(String dept);

	/**
	 * 查询某一部门和岗位下的用户列表
	 *
	 * @param dept
	 * @param position
	 * @return
	 */
	Collection<String> getUsersByDeptAndPosition(String dept, String position);
}

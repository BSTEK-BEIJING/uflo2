 
package com.bstek.uflo.command.impl;

import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.bstek.uflo.command.Command;
import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.task.TaskNoticeCc;

 
public class GetTaskNoticeCcCommand implements Command<List<TaskNoticeCc>> {
	private long processId;
	private String taskName;
	private String businessId;
 
	public GetTaskNoticeCcCommand(long processId){
		this.processId=processId;
	}
	
	public GetTaskNoticeCcCommand( String taskName, String businessId) {
		super();
		this.taskName = taskName;
		this.businessId = businessId;
	}

	@SuppressWarnings("unchecked")
	public List<TaskNoticeCc> execute(Context context) {
		Criteria dateCriteria=context.getSession().createCriteria(TaskNoticeCc.class);
		if(StringUtils.isNotEmpty(taskName)){
			dateCriteria.add(Restrictions.like("taskName", taskName,MatchMode.ANYWHERE));

		}
		if(StringUtils.isNotEmpty(businessId)){
			dateCriteria.add(Restrictions.eq("businessId", businessId));
		}
		if(Objects.nonNull(processId)){
			dateCriteria.add(Restrictions.eq("processId", processId));
			 
		}
		dateCriteria.addOrder(Order.asc("createTime"));
		return dateCriteria.list();
	}

	 

	
}

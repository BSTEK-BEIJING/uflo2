 
package com.bstek.uflo.model.task;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table(name="UFLO_TASK_NOTICE_CC")
public class TaskNoticeCc {
	@Id
	@Column(name="ID_")
	private long id;

	@Column(name="PROCESS_INSTANCE_ID_")
	private long processInstanceId;
	
	@Column(name="PROCESS_ID_")
	private long processId;
	
	@Column(name="PROCESS_NAME_",length=60)
	private String processName;

	@Column(name="TASK_NAME_",length=60)
	private String taskName;
	
	@Column(name="msg_Content_",length=1600)
	private String msgContent;
	
	@Column(name="READ_TIME_")
	private Date readTime;

	@Column(name="SEND_TIME_")
	private Date sendTime;
	
	@Column(name="USER_ID_",length=60)
	private String userId;
	
	@Column(name="MSG_STATE_",length=2)
	private String msgState;
	
	@Column(name="CLASSIFY_",length=60)
	private String classify;
	
	@Column(name="BUSINESS_ID_",length=60)
	private String businessId;
	
	@Column(name="CREATE_TIME")
	private Date createTime;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(long processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public long getProcessId() {
		return processId;
	}
	public void setProcessId(long processId) {
		this.processId = processId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getMsgContent() {
		return msgContent;
	}
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	public Date getReadTime() {
		return readTime;
	}
	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMsgState() {
		return msgState;
	}
	public void setMsgState(String msgState) {
		this.msgState = msgState;
	}
	public String getClassify() {
		return classify;
	}
	public void setClassify(String classify) {
		this.classify = classify;
	}
	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "TaskNoticeCc [id=" + id + ", processInstanceId=" + processInstanceId + ", processId=" + processId
				+ ", processName=" + processName + ", taskName=" + taskName + ", msgContent=" + msgContent
				+ ", readTime=" + readTime + ", sendTime=" + sendTime + ", userId=" + userId + ", msgState=" + msgState
				+ ", classify=" + classify + ", businessId=" + businessId + "]";
	}
	 
}

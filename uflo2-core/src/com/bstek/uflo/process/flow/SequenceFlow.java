/*******************************************************************************
 * Copyright 2017 Bstek
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.bstek.uflo.process.flow;



/**
 * 流程时序接口
 * @author Jacky.gao
 * @since 2013年8月19日
 */
public interface SequenceFlow {

	/**
	 * 获取当前节点信息
	 * @return
	 */
	String getToNode();

	/**
	 * 获取表达式
	 * @return
	 */
	String getExpression();
	
	/**
	 * 获取处理bean
	 * @return
	 */
	String getHandlerBean();

	/**
	 * 获取分支类型
	 * @return
	 */
	ConditionType getConditionType();

	/**
	 * 获取当前时序名称
	 * @return
	 */
	String getName();
}

package com.bstek.uflo.process.handler;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.node.Node;

/**
 * 通知内容-接口
 *
 */
public interface NoticeContentTemplateHandler {
	
	/**
	 *  通知内容处理接口
	 * @param node 节点
	 * @param processInstance 流程实例
	 * @param context 流程上下文
	 * @return
	 */
	Object handle(Node node,ProcessInstance processInstance,Context context);

}

package com.bstek.uflo.process.handler;

import java.util.Collection;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.node.Node;

/**
 * 流程消息通知接口
 * @author hans
 *
 */
public interface NoticeHandler {
	
	/**
	 * 消息被通知人bean，即消息接收人
	 * @param node 节点
	 * @param processInstance 流程实例
	 * @param context 流程上下文
	 * @return
	 */
	Collection<String> handle(Node node,ProcessInstance processInstance,Context context);

}

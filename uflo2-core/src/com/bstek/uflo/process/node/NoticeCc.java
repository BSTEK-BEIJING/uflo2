 
package com.bstek.uflo.process.node;

import java.util.Objects;

import org.apache.commons.lang.StringUtils;

/**
 * 消息通知对象
 * @author admin
 *
 */
public class NoticeCc implements java.io.Serializable{
	private static final long serialVersionUID = -2747945192424126140L;
	 
	
	private String  checked;
	 
	/**
	 * noticeHandlerBean
	 * 通知设置的beanid
	 */
	private String assignmentHandlerBean;
	
	
	/**
	 * 通知人的设置类型 指定参与者：Assignee，指定Bean：Handler  流程发起人：ProcessPromoter
	 */
	private String assignmentType;
	/**
	 * noticeContentTemplateHandlerBean
	 * 通知内容的beanid
	 */
	private String noticeContentTemplateHandlerBean;
	
	/**
	 * 通知内容类型:
	 */
	private String contentType;
 
	/**
	 * 消息内容
	 */
	private String content;
	
	 
	/**
	 * 消息触发条件：任务开始时start，任务结束时end
	 */
	private String fireType;

 

	public String getNoticeContentTemplateHandlerBean() {
		return noticeContentTemplateHandlerBean;
	}

	public void setNoticeContentTemplateHandlerBean(String noticeContentTemplateHandlerBean) {
		this.noticeContentTemplateHandlerBean = noticeContentTemplateHandlerBean;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getAssignmentHandlerBean() {
		return assignmentHandlerBean;
	}

	public void setAssignmentHandlerBean(String assignmentHandlerBean) {
		this.assignmentHandlerBean = assignmentHandlerBean;
	}

	public String getAssignmentType() {
		return assignmentType;
	}

	public void setAssignmentType(String assignmentType) {
		this.assignmentType = assignmentType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFireType() {
		return fireType;
	}

	public void setFireType(String fireType) {
		this.fireType = fireType;
	}
	
	
	public String getChecked() {
		return checked;
	}

	public void setChecked(String checked) {
		this.checked = checked;
	}

	/**
	 * 消息触发条件任务开始
	 * @return
	 */
	public boolean getStart(){
		if(Objects.equals(fireType, "start")){
			return true;
		}
		return false;
		
	}
	/**
	 * 是否勾选了消息配置
	 * @return
	 */
	public boolean checked(){
		if(Objects.equals(checked, "1")){
			return true;
		}
		return false;
		
	}
	public String noticeContentBean(){
		if(StringUtils.isNotBlank(noticeContentTemplateHandlerBean)){
			return noticeContentTemplateHandlerBean;
		}
		return "";
		
	}
	 
}

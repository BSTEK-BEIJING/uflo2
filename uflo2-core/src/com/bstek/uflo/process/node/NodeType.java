package com.bstek.uflo.process.node;

/**
 * 节点类型枚举：动作，人工任务，会签，分支，聚合，动态分支，开始，结束，终止，决策，子流程
 * @author Jacky.gao
 * @since 2017年7月15日
 */
public enum NodeType {
	Action,Task,CountersignTask,Fork,Join,Foreach,Start,End,TerminalEnd,Decision,Subprocess;
}

package com.bstek.uflo.test.notice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.handler.NoticeHandler;
import com.bstek.uflo.process.node.Node;

@Service("testNoticeHandler")
public class TestNoticeHandler implements NoticeHandler{

	@Override
	public Collection<String> handle(Node node, ProcessInstance processInstance, Context context) {
		List<String> users=new ArrayList<String>();
		users.add("user1");
		users.add("user2");
		return users;
	}

}

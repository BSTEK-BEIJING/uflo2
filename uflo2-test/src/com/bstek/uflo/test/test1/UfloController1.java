package com.bstek.uflo.test.test1;

import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.model.task.Task;
import com.bstek.uflo.model.task.TaskState;
import com.bstek.uflo.model.task.TaskType;
import com.bstek.uflo.query.TaskQuery;
import com.bstek.uflo.service.ProcessService;
import com.bstek.uflo.service.StartProcessInfo;
import com.bstek.uflo.service.TaskService;
import com.bstek.uflo.utils.IDGenerator;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ulo测试demo
 *
 * @Author hans
 * @CreateDate 2020-9-19
 */
@Controller
@RequestMapping("/api/uflo1")
public class UfloController1 {

    @Resource(name = ProcessService.BEAN_ID)
    private ProcessService processService;

    @Resource(name = TaskService.BEAN_ID)
    private TaskService taskService;

    /**
     * 开启一个流程实例
     * http://localhost:8080/uflo2/api/uflo1/startProcess
     */
    @ResponseBody
    @RequestMapping(value = "/startProcess", method = RequestMethod.POST)
    public String startProcess() {
        StartProcessInfo info = new StartProcessInfo("admin");
        long id = IDGenerator.getInstance().nextId();
        info.setBusinessId(id + "");
        info.setSubject("流程性能测试1");
        info.setCompleteStartTask(true);
        info.setCompleteStartTaskOpinion("默认同意吧");

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("amount", 100000);

        info.setVariables(variables);
        ProcessInstance processInstance = processService.startProcessByName("测试流程1", info);
        return processInstance.getId() > 0 ? "success" : "failure";
    }

    @ResponseBody
    @RequestMapping(value = "/completeTask", method = RequestMethod.POST)
    public String completeTask() {
        //模拟用户...从待办列表中任选一笔，cliam,complete..
        List<Task> tasks = loadToTask("初审", 3);
        if(tasks==null || tasks.size()==0){
            return  "task not exists";
        }
        Task task=tasks.get(0);
        long taskId=task.getId();
        System.out.println("输入任务主键="+taskId);
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("businessName", "我是测试啊");
        variables.put("businessOwner", "admin");
        if(task.getType().equals(TaskType.Participative)&&task.getAssignee()==null) {
            // 是竞争任务，则领取任务
            taskService.claim(taskId, UserFactory.getRandomUser());
        }
        taskService.start(taskId);
        taskService.complete(taskId, variables);
        return "success";
    }

    public List<Task> loadToTask(String taskName,long processId) {
        TaskQuery query = taskService.createTaskQuery();
        query.addTaskState(TaskState.Created);
        query.addTaskState(TaskState.InProgress);
        query.addTaskState(TaskState.Ready);
        query.addTaskState(TaskState.Suspended);
        query.addTaskState(TaskState.Reserved);
        query.addProcessId(processId);
        query.addOrderDesc("createDate");
        if (StringUtils.isNotBlank(taskName)) {
            query.nameLike("%" + taskName + "%");
        }
        int total = query.count();
        List<Task> tasks = query.list();
        return tasks;
    }

}

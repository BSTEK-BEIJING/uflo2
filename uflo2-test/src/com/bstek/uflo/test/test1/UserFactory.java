package com.bstek.uflo.test.test1;

import org.apache.commons.lang.math.RandomUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试用户构建工厂
 */
public class UserFactory {

    public static List<String> createUsers(){
        List<String> users=new ArrayList<String>();
        for(int i=0;i<100;i++){
            users.add("user"+i);
        }
        return users;
    }

    /**
     * 获取一个随机用户1～100
     * @return
     */
    public static  String getRandomUser(){
        return "user"+RandomUtils.nextInt(100);
    }

    public static void main(String[] args) {
        System.out.println(RandomUtils.nextInt(100));
    }

}

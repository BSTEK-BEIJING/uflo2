package com.bstek.uflo.test;

import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.model.task.Task;
import com.bstek.uflo.model.task.TaskState;
import com.bstek.uflo.query.TaskQuery;
import com.bstek.uflo.service.ProcessService;
import com.bstek.uflo.service.StartProcessInfo;
import com.bstek.uflo.service.TaskService;
import com.bstek.uflo.utils.IDGenerator;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ulo测试demo
 *
 * @Author hans
 * @CreateDate 2020-9-19
 */
@Controller
@RequestMapping("/api/uflo")
public class UfloDemo {

    @Resource(name = ProcessService.BEAN_ID)
    private ProcessService processService;

    @Resource(name = TaskService.BEAN_ID)
    private TaskService taskService;

    /**
     * 部署流程图
     *
     * @param filePath
     * @throws FileNotFoundException
     */
    public void deploy(String filePath) throws FileNotFoundException {
        File f = new File(filePath);
        InputStream inputStream = new FileInputStream(f);
        processService.deployProcess(inputStream);
    }

    /**
     * 开启一个流程实例
     * http://localhost:8080/uflo2/api/uflo/startProcess
     */
    @ResponseBody
    @RequestMapping(value = "/startProcess", method = RequestMethod.POST)
    public String startProcess() {
        StartProcessInfo info = new StartProcessInfo("admin");
        long id = IDGenerator.getInstance().nextId();
        info.setBusinessId(id + "");
        info.setSubject("流程性能测试1");
        info.setCompleteStartTask(true);
        info.setCompleteStartTaskOpinion("默认同意吧");
        ProcessInstance processInstance = processService.startProcessByName("test", info);
        return processInstance.getId() + "";
    }

    /**
     * 处理任务
     *
     * @param taskId
     */
    @ResponseBody
    @RequestMapping(value = "/completeTask/{taskId}", method = RequestMethod.POST)
    public String completeTask(@PathVariable long taskId) {
        System.out.println("输入任务主键="+taskId);
        //模拟用户...从待办列表中任选一笔，cliam,complete..
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("businessName", "我是测试啊");
        variables.put("businessOwner", "admin");
        Task task=taskService.getTask(taskId);
        if(task==null){
            return "task "+taskId+"not exists ！";
        }
        taskService.start(taskId);
        taskService.complete(taskId, variables);
        return "success";
    }

    @ResponseBody
    @RequestMapping(value = "/completeTask", method = RequestMethod.POST)
    public String completeTask() {
        //模拟用户...从待办列表中任选一笔，cliam,complete..
        List<Task> tasks= loadToTask("人工任务1",1);
        if(tasks==null || tasks.size()==0){
            return  "task not exists";
        }
        Task task=tasks.get(0);
        long taskId=task.getId();
        System.out.println("输入任务主键="+taskId);
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("businessName", "我是测试啊");
        variables.put("businessOwner", "admin");
        taskService.start(taskId);
        taskService.complete(taskId, variables);
        return "success";
    }

    public List<Task> loadToTask(String taskName,long processId) {
        TaskQuery query = taskService.createTaskQuery();
        query.addTaskState(TaskState.Created);
        query.addTaskState(TaskState.InProgress);
        query.addTaskState(TaskState.Ready);
        query.addTaskState(TaskState.Suspended);
        query.addTaskState(TaskState.Reserved);
        query.addProcessId(processId);
        query.addOrderDesc("createDate");
        if (StringUtils.isNotBlank(taskName)) {
            query.nameLike("%" + taskName + "%");
        }
        int total = query.count();
        List<Task> tasks = query.list();
        return tasks;
    }

}

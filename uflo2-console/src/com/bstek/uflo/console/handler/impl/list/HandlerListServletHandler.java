/*******************************************************************************
 * Copyright 2017 Bstek
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.bstek.uflo.console.handler.impl.list;

import com.bstek.uflo.console.handler.impl.WriteJsonServletHandler;
import com.bstek.uflo.process.handler.*;
import com.bstek.uflo.process.listener.TaskListener;
import com.bstek.uflo.process.node.FormTemplateProvider;
import com.bstek.uflo.utils.EnvironmentUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 在流程设计器中查询各种handler
 *
 * @author Jacky.gao
 * @since 2016年12月8日
 */
public class HandlerListServletHandler extends WriteJsonServletHandler implements ApplicationContextAware{
	private Map<String,Set<String>> handerMap;
	private boolean debug;
	@Override
	public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String loginUser = EnvironmentUtils.getEnvironment().getLoginUser();
		if (loginUser == null && !debug) {
			throw new IllegalArgumentException("Current run mode is not debug.");
		}
		String handler = req.getParameter("handler");
		if (StringUtils.isEmpty(handler)) {
			//当不指定handler时，输出全部handler
			writeObjectToJson(resp, handerMap);
		} else {
			writeObjectToJson(resp, handerMap.get(handler));
		}
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		handerMap=new HashMap<String,Set<String>>();
		handerMap.put(NodeEventHandler.class.getSimpleName(), applicationContext.getBeansOfType(NodeEventHandler.class).keySet());
		handerMap.put(ProcessEventHandler.class.getSimpleName(), applicationContext.getBeansOfType(ProcessEventHandler.class).keySet());
		handerMap.put(DecisionHandler.class.getSimpleName(), applicationContext.getBeansOfType(DecisionHandler.class).keySet());
		handerMap.put(AssignmentHandler.class.getSimpleName(), applicationContext.getBeansOfType(AssignmentHandler.class).keySet());
		handerMap.put(ConditionHandler.class.getSimpleName(), applicationContext.getBeansOfType(ConditionHandler.class).keySet());
		handerMap.put(ActionHandler.class.getSimpleName(), applicationContext.getBeansOfType(ActionHandler.class).keySet());
		handerMap.put(ForeachHandler.class.getSimpleName(), applicationContext.getBeansOfType(ForeachHandler.class).keySet());
		handerMap.put(ReminderHandler.class.getSimpleName(), applicationContext.getBeansOfType(ReminderHandler.class).keySet());
		handerMap.put(CountersignHandler.class.getSimpleName(), applicationContext.getBeansOfType(CountersignHandler.class).keySet());
		handerMap.put(TaskListener.class.getSimpleName(), applicationContext.getBeansOfType(TaskListener.class).keySet());
		//2022年12月添加流程消息通知bean
		handerMap.put(NoticeHandler.class.getSimpleName(), applicationContext.getBeansOfType(NoticeHandler.class).keySet());
		// 通知内容消息bean
		handerMap.put(NoticeContentTemplateHandler.class.getSimpleName(), applicationContext.getBeansOfType(NoticeContentTemplateHandler.class).keySet());

		Set<String> set=new HashSet<String>();
		for(FormTemplateProvider provider:applicationContext.getBeansOfType(FormTemplateProvider.class).values()){
			set.add(provider.getFormTemplate());
		}
		handerMap.put(FormTemplateProvider.class.getSimpleName(), set);
	}
	
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	@Override
	public String url() {
		return "/handlerlist";
	}
}

package com.bstek.uflo.client.service;

import java.util.List;
import java.util.Map;

import com.bstek.uflo.model.ProcessDefinition;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.model.variable.Variable;
import com.bstek.uflo.query.ProcessInstanceQuery;
import com.bstek.uflo.query.ProcessQuery;
import com.bstek.uflo.query.ProcessVariableQuery;
import com.bstek.uflo.service.StartProcessInfo;

/**
 * 流程操作服务接口
 * 
 * @author Jacky.gao
 * @since 2013年9月22日
 */
public abstract interface ProcessClient {
	public static final String BEAN_ID = "uflo.processClient";

	/**
	 * 查询流程模板信息
	 * 
	 * @param processId
	 *            模板ID
	 * @return
	 */
	ProcessDefinition getProcessById(long processId);

	/**
	 * 查询流程模板信息
	 * 
	 * @param key
	 *            关键字
	 * @return
	 */
	ProcessDefinition getProcessByKey(String key);

	/**
	 * 查询流程模板信息
	 * 
	 * @param processName
	 *            流程名称
	 * @return
	 */
	ProcessDefinition getProcessByName(String processName);

	/**
	 * 查询流程模板信息
	 * 
	 * @param processName
	 *            流程名称
	 * @param version
	 *            版本号
	 * @return
	 */
	ProcessDefinition getProcessByName(String processName, int version);

	/**
	 * 启动流程
	 * 
	 * @param processId
	 *            流程模板主键
	 * @param startProcessInfo
	 *            流程信息
	 * @return
	 */
	ProcessInstance startProcessById(long processId, StartProcessInfo startProcessInfo);

	/**
	 * 启动流程
	 * 
	 * @param key
	 *            流程模板关键字
	 * @param startProcessInfo
	 *            流程信息
	 * @return
	 */
	ProcessInstance startProcessByKey(String key, StartProcessInfo startProcessInfo);

	/**
	 * 启动流程
	 * 
	 * @param processName
	 *            流程模板名称
	 * @param startProcessInfo
	 *            流程信息
	 * @return
	 */
	ProcessInstance startProcessByName(String processName, StartProcessInfo startProcessInfo);

	/**
	 * 删除流程实例
	 * 
	 * @param processInstanceId
	 *            流程实例ID
	 */
	void deleteProcessInstanceById(long processInstanceId);

	/**
	 * 查询流程实例
	 * 
	 * @param processInstanceId
	 *            流程实例ID
	 * @return
	 */
	ProcessInstance getProcessInstanceById(long processInstanceId);

	/**
	 * 查询流程变量
	 * 
	 * @param processInstanceId
	 *            流程实例ID
	 * @return
	 */
	List<Variable> getProcessVariables(long processInsanceId);

	/**
	 * 查询流程变量
	 * 
	 * @param processInsance
	 *            流程实例
	 * @return
	 */
	List<Variable> getProcessVariables(ProcessInstance processInsance);

	/**
	 * 查询流程变量
	 * 
	 * @param key
	 *            流程关键字
	 * @param processInstance
	 *            流程实例
	 * @return
	 */
	Object getProcessVariable(String key, ProcessInstance processInstance);

	/**
	 * 查询流程变量
	 * 
	 * @param key
	 *            流程关键字
	 * @param processInsanceId
	 *            流程实例ID
	 * @return
	 */
	Object getProcessVariable(String key, long processInsanceId);

	/**
	 * 保存流程变量
	 * 
	 * @param processInstanceId
	 *            流程实例ID
	 * @param key
	 *            变量key
	 * @param value
	 *            变量值
	 */
	public abstract void saveProcessVariable(long processInstanceId, String key, Object value);

	/**
	 * 保存流程变量
	 * 
	 * @param processInstanceId
	 *            流程实例ID
	 * @param variables
	 *            变量
	 */
	public abstract void saveProcessVariables(long processInstanceId, Map<String, Object> variables);

	/**
	 * 删除流程变量
	 * 
	 * @param key
	 *            变量key
	 * @param processInstanceId
	 *            流程实例ID
	 */
	public abstract void deleteProcessVariable(String key, long processInstanceId);

	ProcessInstanceQuery createProcessInstanceQuery();

	ProcessVariableQuery createProcessVariableQuery();

	ProcessQuery createProcessQuery();

	/**
	 * 删除流程模板
	 * 
	 * @param processId
	 */
	void deleteProcess(long processId);

	/**
	 * 删除流程模板
	 * 
	 * @param processKey
	 */
	void deleteProcess(String processKey);

	/**
	 * 删除流程模板
	 * 
	 * @param processDefinition
	 */
	void deleteProcess(ProcessDefinition processDefinition);
}

package com.bstek.uflo.client.model;

import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ProcessVariablesInfo")
public class ProcessVariablesInfo
{
  private Map<String, Object> variables;

  public Map<String, Object> getVariables()
  {
    return this.variables;
  }

  public void setVariables(Map<String, Object> variables) {
    this.variables = variables;
  }
}
# 简介
UFLO2是一款纯Java流程引擎，它架构于Spring、Hibernate之上，提供诸如并行、动态并行、串行、会签等各种常见及不常见的业务流程流转功能，支持单机或集群部署；UFLO2采用全新的基于网页的流程模版设计器，打开网页即可完成流程模版的设计与制作。
# 教程
* 在线文档教程：https://www.w3cschool.cn/uflo1/

* uflo2-console工程：流程设计器、待办任务等与用户交互的功能代码
* uflo2-core工程：流程图解析器、流程执行引擎代码

# 联系方式
* qq：351715466
* email：hans.han@bstek.com

# 项目安装与运行
## 安装
* 直接克隆到本地
* 导入到ide中
* 项目依赖关系：uflo2-console工程依赖uflo2-core和uflo2-test下的lib；uflo2-core依赖于uflo2-test下的lib；uflo2-test依赖于uflo2-core和uflo2-console

## 运行
### 1.maven版本
* 因某些原因，master分支已由maven结构转换成传统web结构，喜欢maven的开发者可以下载其它分支

### 2.传统web工程
* 直接右键以jetty插件方式启动
* 外置容器tomcat插件方式启动

### 3.在浏览器中访问以下页面
* 流程设计器页面：http://localhost:8080/uflo2-test/uflo/designer

![](./doc/设计器.png)

* 待办任务：http://localhost:8080/uflo2-test/uflo/todo

![](./doc/待办任务.png)

* 流程监控中心：http://localhost:8080/uflo2-test/uflo/central

![](./doc/流程监控.png)

* 查看流程图

![](./doc/查看流程图.png)